﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeworkBD
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var db = new Univer.UniverContext())
            {
                db.Database.Log = (s => System.Diagnostics.Debug.WriteLine(s));
                var students = db.Student;
                   
                foreach (var student in students)
                    Console.WriteLine($"{student.Sname}");
                Console.ReadKey();
            }
        }
    }
}
