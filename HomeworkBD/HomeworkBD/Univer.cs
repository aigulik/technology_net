﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeworkBD
{
    class Univer
    {
        [Table("Institute")]
        public class Institute
        {
            [Key]
            public int InstituteId { get; set; }
            public string Iname { get; set; }

            public virtual ICollection<Group> Group { get; set; } = new List<Group>();
            public virtual ICollection<Teacher> Teacher { get; set; } = new List<Teacher>();
        }
        [Table("Group")]
        public class Group
        {
            [Key]
            public int GroupId { get; set; }
            public string Gname { get; set; }

            public int? InstituteId { get; set; }
            public virtual Institute Institute { get; set; }
            
            public virtual ICollection<Student> Student { get; set; } = new List<Student>();

        }
        [Table("Teacher")]
        public class Teacher
        {
            [Key]
            public int TeacherId { get; set; }
            public string Tname  { get; set; }
           

            public int? InstituteId { get; set; }
            public virtual Institute Institute { get; set; }

            public virtual ICollection<Course> Course { get; set; } = new List<Course>();


        }
        [Table("Course")]
        public class Course
        {
            [Key]
            public int CourseId { get; set; }
            public string Cname { get; set; }
            public string Cinfo { get; set; }

            public int? TeacherId { get; set; }
            public virtual Teacher Teacher { get; set; }
            public int? StudentId { get; set; }
            public virtual Student Student { get; set; }


        }
        [Table("Student")]
        public class Student
        {
            public int StudentId { get; set; }
            public string Sname { get; set; }
            public int Sage { get; set; }

            public int? GroupId { get; set; }
            public virtual Group Group { get; set; }
            public int? CourseId { get; set; }
            public virtual Course Course { get; set; }

        }
        public class UniverContextInitializer : DropCreateDatabaseAlways<UniverContext>
        {
            protected override void Seed(UniverContext db)
            {
                var i1 = new Institute { Iname = "KFU" };
                db.Institute.Add(i1);

                var g = new List<Group>() {
                new Group {Gname="11-405", Institute=i1 },
                new Group {Gname="11-406", Institute=i1  },

            };
                db.Group.AddRange(g);
                db.SaveChanges();

                var t = new List<Teacher>()
                {
                    new Teacher {Tname="Marchenko", Institute=i1 },
                    
                };
                db.Teacher.AddRange(t);
                db.SaveChanges();

                var c = new List<Course>()
                {
                    new Course {Cname="2", Cinfo="info", Teacher=t, Student=s },
                };
                db.Course.AddRange(c);
                db.SaveChanges();

                var s = new List<Student>()
                {
                    new Student {Sname="Aigul", Sage=21,Group=g, Course=c   },
                };
                db.Student.AddRange(s);
                db.SaveChanges();
            }
        }
        public class UniverContext : DbContext
        {

            static UniverContext()
            {
                Database.SetInitializer<UniverContext>(new UniverContextInitializer());
            }

            public UniverContext()
                : base("StoreDB")
            {
                AppDomain.CurrentDomain.SetData("DataDirectory", System.IO.Directory.GetCurrentDirectory());
            }
            public DbSet<Institute> Institute { get; set; }
            public DbSet<Group> Group { get; set; }
            public DbSet<Teacher> Teacher { get; set; }
            public DbSet<Course> Course { get; set; }
            public DbSet<Student> Student { get; set; }


        }
    }
}
